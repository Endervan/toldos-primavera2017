
<div class="container-fluid rodape top50">
	<div class="row">


		<div class="container">
			<div class="row bottom10">

				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-4 logo_rodape">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->


				<div class="col-8 padding0 menu_rodape top40">
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
					<ul class="nav justify-content-end">
						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">HOME
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico" or Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">FALE CONOSCO
							</a>
						</li>


						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">ORÇAMENTO
							</a>
						</li>

					</ul>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
				</div>

				<!-- ======================================================================= -->
				<!-- ONDE ESTAMOS   -->
				<!-- ======================================================================= -->
				<div class="col-4 endereco_rodape padding0 top35">
					<h2><span>ONDE ESTAMOS</span></h2>
					<div class="media top30">
						<img class="d-flex mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_endereco_amarelo.png" alt="">
						<div class="media-body">
							<p><span><?php Util::imprime($config[endereco]); ?></span></p>
						</div>
					</div>

				</div>
				<!-- ======================================================================= -->
				<!-- ONDE ESTAMOS   -->
				<!-- ======================================================================= -->



				<div class="col-6 top35 endereco_rodape">
					<h2><span>FALE CONOSCO</span></h2>
					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
					<div class="row top30">
						<div class="col-6 media">
							<img class="d-flex mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_amarelo.png" alt="">
							<div class="media-body align-self-center">
								<h2><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h2>
							</div>
						</div>
						<?php if (!empty($config[telefone2])): ?>
							<div class="col-6 media">
								<img class="d-flex mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_amarelo.png" alt="">
								<div class="media-body align-self-center">
									<h2><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
								</div>
							</div>
						<?php endif; ?>
					</div>

					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
				</div>



				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->
				<div class="col-2 redes top60">
					<div class="row">
						<div class="col text-right padding0 top30">
							<?php if ($config[google_plus] != "") { ?>
								<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
									<i class="fa fa-google-plus-official pt20 fa-2x right15"></i>
								</a>
							<?php } ?>
						</div>
						<?php 	/*
						<?php if ($config[facebook] != "") { ?>
						<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
						<i class="fa fa-facebook-official fa-2x top20 right15"></i>
						</a>
						<?php } ?>

						<?php if ($config[instagram] != "") { ?>
						<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
						<i class="fa fa-instagram fa-2x top20 right15"></i>
						</a>
						<?php } ?>

						*/ ?>
						<div class="col">
							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
							</a>
						</div>

					</div>
				</div>
				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->




			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row rodape-preto lato">
		<div class="col-12 text-center top10 bottom10">
			<h5>© Copyright  TOLDOS PRIMAVERA</h5>
		</div>
	</div>
</div>
