
<div class="container bg_topo">
  <div class="row">

    <div class="col-3 logo">
      <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
      </a>
    </div>

    <div class="col-9">

      <div class="row">
        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->
        <div class="col-12 top10 menu">
          <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
          <div id="navbar">
            <ul class="nav navbar-nav navbar-right align-self-center">
              <li >
                <a class=" top10 <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/">HOME
                </a>
              </li>

              <li >
                <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
                </a>
              </li>



              <li>
                <a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "orcamento" ){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
                </a>
              </li>


              <li>
                <a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico" or Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
                </a>
              </li>




              <li >
                <a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO
                </a>
              </li>

              <li>



                <!--  ==============================================================  -->
                <!--CARRINHO-->
                <!--  ==============================================================  -->
                <div class=" carrinho">
                  <div class="dropdown show">
                    <a class="btn btn_topo texto-branco" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      ORÇAMENTO
                      <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    </a>

                    <div class="dropdown-menu topo-meu-orcamento" style="float: right;" aria-labelledby="dropdownMenuLink">

                      <?php
                      if(count($_SESSION[solicitacoes_produtos]) > 0){
                        for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                          $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                          ?>
                          <tr class="middle">
                            <div class="row lista-itens-carrinho">
                              <div class="col-2">
                                <?php if(!empty($row[imagem])): ?>
                                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                <?php endif; ?>
                              </div>
                              <div class="col-8">
                                <h1><?php Util::imprime($row[titulo]) ?></h1>
                              </div>
                              <div class="col-1">
                                <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                              </div>
                              <div class="col-12"><div class="borda_carrinho top5">  </div> </div>
                            </div>
                          </tr>
                          <?php
                        }
                      }
                      ?>

                      <!--  ==============================================================  -->
                      <!--sessao adicionar servicos-->
                      <!--  ==============================================================  -->
                      <?php
                      if(count($_SESSION[solicitacoes_servicos]) > 0)
                      {
                        for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                        {
                          $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                          ?>
                          <div class="row lista-itens-carrinho">
                            <div class="col-2">
                              <?php if(!empty($row[imagem])): ?>
                                <!-- <img class="carrinho_servcos" src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" alt="" /> -->
                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                              <?php endif; ?>
                            </div>
                            <div class="col-8">
                              <h1><?php Util::imprime($row[titulo]) ?></h1>
                            </div>
                            <div class="col-1">
                              <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                            </div>
                            <div class="col-12"><div class="borda_carrinho top5">  </div> </div>

                          </div>
                          <?php
                        }
                      }
                      ?>

                      <div class="col-12 text-right top10 bottom20">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn_orcamento" >
                          ENVIAR ORÇAMENTO
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <!--  ==============================================================  -->
                <!--CARRINHO-->
                <!--  ==============================================================  -->
              </li>

            </ul>
          </div><!--/.nav-collapse -->

        </div>
        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->

      </div>


      <!-- ======================================================================= -->
      <!-- telefones  -->
      <!-- ======================================================================= -->
      <div class="telefone_topo">
        <div class="row top20">
          <div class="col-4"></div>
          <div class="col padding0 media">
            <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_amarelo.png" alt="">
            <div class="media-body align-self-center">
              <h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h2>
            </div>
          </div>
          <?php if (!empty($config[telefone2])): ?>
            <div class="col padding0 media">
              <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_amarelo.png" alt="">
              <div class="media-body align-self-center">
                <h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>

      <!-- ======================================================================= -->
      <!-- telefones  -->
      <!-- ======================================================================= -->









    </div>


  </div>
</div>
