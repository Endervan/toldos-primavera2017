<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center  no-repeat;
  }
  </style>

  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>



</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">

    <div class="col-12 top20 text-center titulo_emp">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6 class="text-left">CONHEÇA MAIS A</h6>
      <h6><span>TOLDOS PRIMAVERA</span></h6>
      <amp-img class="top10 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>

  <div class="row bg_cinza">
    <div class="bloco_amarelo">  </div>

    <div class="col-11 col-offset-1 text-center">
      <amp-carousel id="carousel-with-preview"
      width="400"
      height="350"
      layout="responsive"
      type="slides"
      autoplay
      delay="4000"
      >

      <?php
      $i = 0;
      $result = $obj_site->select("tb_banners", "AND tipo_banner = 3 ORDER BY rand() LIMIT 6");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
          ?>


          <amp-img
          on="tap:lightbox1"
          role="button"
          tabindex="0"

          src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
          layout="responsive"
          width="400"
          height="300"
          alt="<?php echo Util::imprime($row[titulo]) ?>">

        </amp-img>
        <?php
      }
      $i++;
    }
    ?>

  </amp-carousel>
  <amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>
</div>
</div>

<div class="row bg_cinza">
  <!-- ======================================================================= -->
  <!-- conheca nossa empresa  -->
  <!-- ======================================================================= -->
  <div class="col-12 top20 bottom50">
    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
    <div class="desc_empresa">
      <h2><?php Util::imprime($row[titulo]); ?></h2>
      <div class="top35">  <p><?php Util::imprime($row[descricao]); ?></p></div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- conheca nossa empresa  -->
  <!-- ======================================================================= -->
</div>


<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
<div class="row ">
  <div class="col-10 top20">


    <amp-img
    layout="responsive"
    height="80"
    width="334"
    src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtoseservicos.png" alt="" >
  </amp-img>
</div>
</div>

<div class="row bottom100">

  <?php
  $i=0;
  $result = $obj_site->select("tb_produtos","order by rand() limit 1");
  if(mysql_num_rows($result) > 0){

    while($row = mysql_fetch_array($result)){
      ?>

      <div class="col-12 bottom30">
        <div class="card">
          <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <amp-img
            layout="responsive"
            height="150"
            width="480"
            src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
          </amp-img>
        </a>
        <div class="card-body text-center bg_amarelo_empresa">
          <div class="text-uppercase">
            <h3><?php Util::imprime($row[titulo]); ?></3>
            </div>
            <div class=" bottom20 col-10 col-offset-1 text-center padding0 btn_sobreposto_empresa">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_empresa">
                <amp-img
                layout="responsive"
                height="65"
                width="373"
                src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/servicos_btn.png" alt="" >
              </amp-img>
            </a>
          </div>

        </div>
        <div class="col-12">  </div>
      </div>
    </div>
    <?php
    if($i == 0){
      echo '<div class=" top20 clearfix"></div>';
      $i = 0;
    }else{
      $i++;
    }

  }
}
?>



<?php
$i=0;
$result = $obj_site->select("tb_servicos","order by rand() limit 2");
if(mysql_num_rows($result) > 0){

  while($row = mysql_fetch_array($result)){
    ?>

    <div class="col-12 top30 bottom30">
      <div class="card">
        <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
          <amp-img
          layout="responsive"
          height="150"
          width="480"
          src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
        </amp-img>
      </a>
      <div class="card-body text-center bg_amarelo_empresa">
        <div class="text-uppercase"><h3><?php Util::imprime($row[titulo]); ?></3></div>
          <div class="col-10 col-offset-1 text-center padding0 btn_sobreposto_empresa_servico">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="MAIS DETALHES" class="btn btn_empresa">
              <amp-img
              layout="responsive"
              height="65"
              width="373"
              src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos_btn.png" alt="" >
            </amp-img>

          </a>
        </div>

      </div>
      <div class="col-12">  </div>

    </div>
  </div>
  <?php
  if($i == 1){
    echo '<div class="clearfix"></div>';
    $i = 0;
  }else{
    $i++;
  }

}
}
?>



</div>
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
