<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center  no-repeat;
  }
  </style>

  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>




</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">

    <div class="col-12 top20 localizacao-pagina1 text-center">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6 class="text-left">CONFIRA TODOS OS</h6>
      <h6><span>NOSSOS SERVICOS</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>


  <div class="row">
    <div class="col-12 top15">
      <a class="col-6 padding0" href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
        <amp-img
        layout="responsive"
        src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/ligue_agora.png" alt="" height="50" width="220">
      </amp-img>
    </a>
    <a class="col-6 padding0" href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
      <amp-img

      layout="responsive"
      src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/solicitar_orcamento.png" alt="" height="50" width="220">
    </amp-img>
  </a>
</div>


</div>




<!--  ==============================================================  -->
<!--   NOSSOS SERVIÇOS -->
<!--  ==============================================================  -->
<div class="row servicos">

  <?php
  $result = $obj_site->select("tb_servicos");
  if(mysql_num_rows($result) > 0){
    $i = 0;
    while($row = mysql_fetch_array($result)){
      if ($i == 0) {
        $order = 'order-1';
        $order1 = 'order-12';
        $div = 'container_metade_direita';
        $i++;
      }else{
        $order = 'order-12';
        $order1 = 'order-1';
        $div = 'container_metade_esquerda';
        $i = 0;
      }
      ?>

      <div class="relativo">
        <div class="<?php echo $div; ?>"></div>
        <div class="row">
          <div class="col-7 top25 <?php echo $order;?>">
            <h3 class="text-uppercase top15"><span><?php Util::imprime($row[titulo]); ?></span></h3>
            <div class="top15"><h4><?php Util::imprime($row[subtitulo]); ?></h4></div>
            <div class="top10"><p><?php Util::imprime($row[descricao],100); ?></p></div>
          </div>
          <div class="col-5 padding0 <?php echo $order1;?> top15">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <amp-img
              layout="responsive"
              height="150"
              width="200"
              src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
            </amp-img>
          </a>
        </div>
      </div>
    </div>

    <div class="col-12 top10 bottom40 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais_servicos">MAIS DETALHES</a>
    </div>

    <?php
    if ($i==0) {
      echo "<div class='clearfix'>  </div>";
    }else {
      $i++;
    }
  }
}
?>


</div>
<!--  ==============================================================  -->
<!--   NOSSOS SERVIÇOS -->
<!--  ==============================================================  -->





<!-- ======================================================================= -->
<!-- VEJA TAMBEM    -->
<!-- ======================================================================= -->
<div class="row bottom100">

  <div class="col-8 top50 veja">
    <amp-img
    layout="responsive"
    height="70" width="325"
    src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/veja_tambem.png" alt="" >
  </amp-img>

</div>

<?php
$i = 0;
$result = $obj_site->select("tb_produtos","limit 2");
if(mysql_num_rows($result) == 0){
  echo "<h2 class='bg-info top25' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
}else{
  while ($row = mysql_fetch_array($result))
  {
    $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
    $row_categoria = mysql_fetch_array($result_categoria);
    ?>

    <div class="col-6 top15 nossos_produtos">

      <div class="card">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
          <amp-img
          layout="responsive"
          height="300" width="426"
          src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
        </amp-img>
      </a>
      <div class="card-body text-center bg_produtos_home1">
        <div class="top5  desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[titulo],60); ?></h1></div>
        <div class="btn_sobreposto1">
          <div class="col-8">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <amp-img
              src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/mais_detalhes.jpg"
              width="128"
              height="40"
              layout="responsive"
              alt="<?php echo Util::imprime($row[titulo]) ?>">
            </amp-img>
          </a>
        </div>
        <div class="col-4">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $row[0] ?>&tipo_orcamento=produto" title="SOLICITAR UM ORÇAMENTO">
            <amp-img
            src="<?php echo Util::caminho_projeto() ?>/imgs/btn_orcamento.jpg"
            width="58"
            height="45"
            layout="responsive"
            alt="<?php echo Util::imprime($row[titulo]) ?>">
          </amp-img>
        </a>
      </div>

    </div>
  </div>
</div>
</div>
<?php
if($i == 1){
  echo '<div class="clearfix"></div>';
  $i = 0;
}else{
  $i++;
}

}
}
?>


</div>
<!-- ======================================================================= -->
<!-- VEJA TAMBEM    -->
<!-- ======================================================================= -->


<?php require_once("../includes/rodape.php") ?>

</body>



</html>
