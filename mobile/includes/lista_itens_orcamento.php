
<div class="row ">
  <div class="lista-produto-titulo">
    <h1 class="">ITENS SELECIONADOS(<?php echo count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]); ?>)</h1>


    <table class="input100 bg_branco tabela-orcamento ">
      <tbody>

        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>

        <?php
        if(count($_SESSION[solicitacoes_produtos]) > 0){
          for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
            $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
            ?>
            <tr>
              <td>
                <amp-img
                height="86" width="100"
                src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]) ?>">
              </amp-img>
            </td>
            <td class="text-uppercase"><?php Util::imprime($row[titulo]); ?></td>
            <td class="text-center">
              <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
              <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
            </td>
            <td class="text-center">
              <a href="?action=del&id=<?php echo $i; ?>&tipo_orcamento=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                <i class="fa fa-times-circle fa-2x"></i>
              </a>
            </td>
          </tr>
          <?php
        }
      }
      ?>

      <?php
      if(count($_SESSION[solicitacoes_servicos]) > 0){
        for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
          $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
          ?>
          <tr>
            <td>
              <amp-img
              height="86" width="100"
              src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
            </amp-img>
          </td>
          <td class="text-uppercase"><?php Util::imprime($row[titulo]); ?></td>
          <td class="text-center">
            <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
            <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
          </td>
          <td class="text-center">
            <a href="?action=del&id=<?php echo $i; ?>&tipo_orcamento=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
              <i class="fa fa-times-circle fa-2x"></i>
            </a>
          </td>
        </tr>
        <?php
      }
    }
    ?>


  </tbody>
</table>

</div>
</div>
