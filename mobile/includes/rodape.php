<div class="row">
    <div class="clearfix"></div>
    <div class="rodape col-12">
        <div class="col-6 top5">
            <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
                <i class="fa fa-phone" aria-hidden="true"></i> LIGAR AGORA
            </a>
        </div>
        <div class="col-6 top5 text-right">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento">
                MEU ORÇAMENTO <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>
