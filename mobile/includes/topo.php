<div class="row bg_topo">
    <div class="col-2 ">
        <?php
        if(empty($voltar_para)){
            $link_topo = Util::caminho_projeto()."/mobile/";
        }else{
            $link_topo = Util::caminho_projeto()."/mobile/".$voltar_para;
        }
        ?>
        <a href="<?php echo $link_topo  ?>"><i class="fa fa-arrow-left fa-2x btn-topo" aria-hidden="true"></i></a>
    </div>
    <div class="col-8 topo">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile">
        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo-paginas.png" alt="Home" height="75" width="226"></amp-img>
      </a>
    </div>
    <div class="col-2 text-right">
        <button on="tap:sidebar.toggle" class="ampstart-btn caps m2 btn-topo"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></button>
    </div>
</div>



<amp-sidebar id="sidebar" layout="nodisplay" side="left" class="menu-mobile-principal">
  <ul class="menu-mobile">
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile"> <i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa"> <i class="fa fa-building-o" aria-hidden="true"></i> A Empresa</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos"> <i class="fa fa-suitcase" aria-hidden="true"></i> Produtos</a></li>
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos"> <i class="fa fa-server" aria-hidden="true"></i> Serviços</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco"> <i class="fa fa-envelope" aria-hidden="true"></i> Fale Conosco</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco"> <i class="fa fa-group" aria-hidden="true"></i> Trabalhe Conosco</a></li>
  </ul>
</amp-sidebar>
