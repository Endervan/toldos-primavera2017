<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success.hide-inputs > input {
    display: none;
  }


  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center  no-repeat;
  }
  </style>

  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>


</head>





<body class="bg-interna">


  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once("../includes/topo.php")
  ?>


  <div class="row">

    <div class="col-12 top20 localizacao-pagina1 text-center titulo_emp">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6 class="text-left">ENTRE EM CONTATO</h6>
      <h6><span>TRABALHE CONOSCO</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>

  <div class="row">
    <div class="col-12 top15">
      <a class="col-6 padding0" href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
        <amp-img
        class="right2"
        layout="responsive"
        src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/ligue_agora.png" alt="" height="50" width="220">
      </amp-img>




    </a>
    <a class="col-6 padding0" href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
      <amp-img

      layout="responsive"
      src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/solicitar_orcamento.png" alt="" height="50" width="220">
    </amp-img>
  </a>
  </div>

  <div class="col-6">
  <h4 class="top5">
    <amp-fit-text
    width="221"
    height="50"
    layout="responsive"
    min-font-size="21">
    <?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>
  </amp-fit-text>
  </h4>


  </div>
  </div>


<div class="row bottom10">

  <div class="col-12">


    <?php
    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_GET[nome])){
      $texto_mensagem = "
      Nome: ".($_GET[nome])." <br />
      Email: ".($_GET[email])." <br />
      Sexo: ".($_GET[sexo])." <br />
      Nascimento: ".($_GET[nascimento])." <br />
      Grau de Instrução: ".($_GET[grau_instrucao])." <br />
      Endereco: ".($_GET[endereco])." <br />
      Area Predentido: ".($_GET[area_predentido])." <br />
      Cargo Predentido: ".($_GET[cargo_predentido])." <br />
      Curricúlo: ".($_GET[curriculo])." <br />

      Mensagem: <br />
      ".(nl2br($_GET[mensagem]))."
      ";


      Util::envia_email($config[email], utf8_decode("$_GET[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_GET[nome]), $_GET[email]);
      Util::envia_email($config[email_copia], utf8_decode("$_GET[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_GET[nome]), $_GET[email]);

      Util::alert_bootstrap("Obrigado por entrar em contato.");
      unset($_GET);
    }
    ?>


    <form method="get" action="envia.php" target="_top">


      <div class="">
        <h1><span>ENVIE SEUS DADOS PROFISSIONAIS</span></h1>
      </div>


      <div class="ampstart-input inline-block relative m0 p0 mb3 trabalhe_conosco">

        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="nome"  placeholder="NOME " required>
          <span class="fa fa-user form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="email" class="input-form input100 block border-none p0 m0" name="email"  placeholder="EMAIL" required>
          <span class="fa fa-envelope form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone"  placeholder="TELEFONE" required>
          <span class="fa fa-phone form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="sexo" placeholder="SEXO"  required>
          <span class="fa fa-star form-control-feedback"></span>
        </div>

        <div class="relativo curriculo">
          <b class="">NASCIMENTO</b>
          <input class="input-form input100 block border-none p0 m0" name="nascimento"  type="date"  placeholder="NASCIMENTO" required>
          <span class="fa fa-calendar form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="grau_instrucao" placeholder="GRAU DE INSTRUÇÃO" required>
          <span class="fa fa-list-alt form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="endereco" placeholder="ENDEREÇO" required>
          <span class="fa fa-map-marker form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="area_predentido" placeholder="ÁREA PRETENDIDO" required>
          <span class="fa fa-folder-open form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="cargo_predentido" placeholder="CARGO PRETENTIDO" required>
          <span class="fa fa-folder-open form-control-feedback"></span>
        </div>

        <div class="relativo curriculo">
          <b class="">CURRÍCULO</b>
          <input type="file" class="input-form input100 block border-none p0 m0 pt20 curriculo" name="curriculo" placeholder="CURRÍCULO" required>
          <span class="fa fa-file-text form-control-feedback"></span>
        </div>


        <div class="relativo">
          <textarea name="mensagem" class="input-form1 input100 campo-textarea" placeholder="MENSAGEN"></textarea>
          <span class="fa fa-pencil form-control-feedback"></span>
        </div>

      </div>

      <div class="col-12 ">
        <div class="relativo text-center">
          <input type="submit" value="ENVIAR" class="ampstart-btn caps btn btn_formulario col-5">
        </div>
      </div>


      <div submit-success>
        <template type="amp-mustache">
          Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
        </template>
      </div>

      <div submit-error>
        <template type="amp-mustache">
          Houve um erro, {{name}} por favor tente novamente.
        </template>
      </div>

    </form>
  </div>

</div>


<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class="row">
  <div class="col-6">
    <amp-img class="top10 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/onde.png" alt=""
      layout="responsive"
      height="45"
      width="160">
    </amp-img>
  </div>


  <div class="col-12 top10 bottom25 padding0">
    <amp-iframe
    width="480"
    height="304"
    layout="responsive"
    sandbox="allow-scripts allow-same-origin allow-popups"
    frameborder="0"
    src="<?php Util::imprime($config[src_place]); ?>">
  </amp-iframe>
</div>
</div>

<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
