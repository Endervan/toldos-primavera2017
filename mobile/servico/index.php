
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/servicos/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!doctype html>
<html amp lang="pt-br">
<head>
	<?php require_once("../includes/head.php"); ?>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

	<style amp-custom>
	<?php require_once("../css/geral.css"); ?>
	<?php require_once("../css/topo_rodape.css"); ?>
	<?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



	<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11); ?>
	.bg-interna{
		background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center  no-repeat;
	}
	</style>

	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
	<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>



</head>

<body class="bg-interna">

	<?php
	$voltar_para = 'servicos'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php")
	?>

	<div class="row">

		<div class="col-12 top20 localizacao-pagina1 text-center titulo_emp">
			<!-- ======================================================================= -->
			<!-- TITULO PAGINA  -->
			<!-- ======================================================================= -->
			<h6 class="text-left">CONFIRA TODOS OS</h6>
			<h6><span>NOSSOS SERVIÇOS</span></h6>
			<amp-img class="top10 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
			<!-- ======================================================================= -->
			<!-- TITULO PAGINA  -->
			<!-- ======================================================================= -->
		</div>
	</div>


	  <div class="row">
	    <div class="col-12 top15">
	      <a class="col-6 padding0" href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
	        <amp-img
	        layout="responsive"
	        src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/ligue_agora.png" alt="" height="50" width="220">
	      </amp-img>
	    </a>

	</div>


	</div>


<div class="row">
	<div class="col-12 top15">

		<amp-carousel id="carousel-with-preview"
		width="480"
		height="250"
		layout="responsive"
		type="slides"
		autoplay
		delay="4000"
		>

		<?php
		$i = 0;
		$result = $obj_site->select("tb_galerias_servicos", "and id_servico = $dados_dentro[idservico]");
		if (mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_array($result)){
				?>


				<amp-img
				on="tap:lightbox1"
				role="button"
				tabindex="0"

				src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
				layout="responsive"
				width="480"
				height="400"
				alt="<?php echo Util::imprime($row[titulo]) ?>">

			</amp-img>
			<?php
		}
		$i++;
	}
	?>

</amp-carousel>

<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>




<div class="carousel-preview">

	<?php
	$i = 0;
	$result = $obj_site->select("tb_galerias_servicos", "and id_servico = $dados_dentro[idservico]");
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_array($result)){
			?>

			<button on="tap:carousel-with-preview.goToSlide(index=<?php echo $i++; ?>)">

				<amp-img class="col-3"
				src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
				width="40"
				height="40"
				alt="<?php echo Util::imprime($row[titulo]) ?>">
			</amp-img>
		</button>

		<?php
	}
	$i++;
}
?>
</div>

</div>

</div>




<div class="row servico_dentro">
	<div class="col-12 top25">
		<h5 class="text-uppercase"><?php echo Util::imprime($dados_dentro[titulo]) ?></h5>
	</div>
	<div class="col-12 top15 dicas_dentro ">
		<div>	<p><?php echo Util::imprime($dados_dentro[descricao]) ?></p></div>
	</div>
</div>

<div class="row top30">

<div class="col-8">
	<a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $dados_dentro[0] ?>&tipo_orcamento=servico" title="SOLICITAR UM ORÇAMENTO">
		<amp-img
		layout="responsive"
		src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/solicitar_orcamento.png" alt="" height="50" width="220">
	</amp-img>
</a>
</div>

</div>


<!-- ======================================================================= -->
<!-- VEJA TAMBEM    -->
<!-- ======================================================================= -->
<div class="row bottom100">

	<div class="col-8 top50 veja">
		<amp-img
		layout="responsive"
		height="70" width="325"
		src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/veja_tambem.png" alt="" >
	</amp-img>

</div>

<?php
$i = 0;
$result = $obj_site->select("tb_produtos","limit 2");
if(mysql_num_rows($result) == 0){
	echo "<h2 class='bg-info top25' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
}else{
	while ($row = mysql_fetch_array($result))
	{
		$result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
		$row_categoria = mysql_fetch_array($result_categoria);
		?>

		<div class="col-6 top15 nossos_produtos">

			<div class="card">
				<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
					<amp-img
					layout="responsive"
					height="300" width="426"
					src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
				</amp-img>
			</a>
			<div class="card-body text-center bg_produtos_home1">
				<div class="top5  desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[titulo],60); ?></h1></div>
				<div class="btn_sobreposto1">
					<div class="col-8">
						<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
							<amp-img
							src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/mais_detalhes.jpg"
							width="128"
							height="33"
							layout="responsive"
							alt="<?php echo Util::imprime($row[titulo]) ?>">
						</amp-img>
					</a>
				</div>
				<div class="col-4">
					<a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $row[0] ?>&tipo_orcamento=produto" title="SOLICITAR UM ORÇAMENTO">
						<amp-img
						src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/card.png"
						width="56"
						height="40"
						layout="responsive"
						alt="<?php echo Util::imprime($row[titulo]) ?>">
					</amp-img>
				</a>
			</div>

		</div>
	</div>
</div>
</div>
<?php
if($i == 1){
	echo '<div class="clearfix"></div>';
	$i = 0;
}else{
	$i++;
}

}
}
?>


</div>
<!-- ======================================================================= -->
<!-- VEJA TAMBEM    -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
