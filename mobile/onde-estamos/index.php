<?php
$ids = explode("/", $_GET[get1]);

require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10); ?>
        .bg-interna{
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center  no-repeat;
        }
    </style>


    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>

</head>

<body class="bg-interna">

<?php
$voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
require_once("../includes/topo.php") ?>


<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">

    <div class="col-12 top20 localizacao-pagina1 text-center">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6 class="text-left">ONDE</h6>
      <h6><span>ESTAMOS</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>


  
  <div class="row">
    <div class="col-12 top15">
          <a class="col-6 padding0" href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
            <amp-img
            layout="responsive"
            src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/ligue_agora.png" alt="" height="50" width="220">
          </amp-img>
        </a>
      <a class="col-6 padding0" href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
          <amp-img

          layout="responsive"
          src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/solicitar_orcamento.png" alt="" height="50" width="220">
        </amp-img>
      </a>
  </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->




 <?php require_once("../includes/unidades.php"); ?>





<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->
<?php //require_once("../includes/veja.php") ?>
<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
