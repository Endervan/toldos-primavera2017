
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("./includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("./css/geral.css"); ?>
  <?php require_once("./css/topo_rodape.css"); ?>
  <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  </style>

  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>

</head>



<body class="bg-index">


  <div class="row">
    <div class="col-12 text-center topo">
      <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" height="76" width="480"></amp-img>
    </div>
  </div>


  <div class="row font-index">
    <div class="col-12 top15 bottom15">
      <div class=" text-center">
        <h1>TENDAS E COBERTURAS</h1>
        <h2 class="top10">ALTO PADRÃO,ESTILO E DURABILIDADE PARA SEU AMBIENTE</h2>
      </div>
    </div>





  <div class="col-4 top20 text-center">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
      <div class="col-10 col-offset-1 index-bg-icones">
        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_home02.png" alt="PRODUTOS" width="55" height="50" ></amp-img>
      </div>
      <p>PRODUTOS</p>
    </a>
  </div>
  <div class="col-4 top20 text-center">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">
      <div class="col-10 col-offset-1 index-bg-icones">
        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_home03.png" alt="SERVIÇOS" width="55" height="50" ></amp-img>
      </div>
      <p>SERVIÇOS</p>
    </a>
  </div>

  <div class="col-4 top20 text-center">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">
      <div class="col-10 col-offset-1 index-bg-icones">
        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_home04.png" alt="A EMPRESA" width="55" height="50" ></amp-img>
      </div>

      <p class="clearfix">A EMPRESA</p>
    </a>
  </div>
  <div class="col-4 top20 text-center">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">
      <div class="col-10 col-offset-1 index-bg-icones">
        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_home05.png" alt="FALE CONOSCO" width="55" height="50" ></amp-img>
      </div>
      <p>FALE CONOSCO</p>
    </a>
  </div>
  <div class="col-4 top20 text-center">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento">
      <div class="col-10 col-offset-1 index-bg-icones">
        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_home06.png" alt="ORÇAMENTO" width="55" height="50" ></amp-img>
      </div>
      <p>ORÇAMENTO</p>
    </a>
  </div>

  <div class="col-4 top20 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/onde-estamos" >
          <div class="col-10 col-offset-1 index-bg-icones">
              <i class="fa fa-map-marker" aria-hidden="true"></i>
          </div>
          <p>ONDE ESTAMOS</p>
      </a>
  </div>

</div>


<?php require_once("./includes/rodape.php") ?>


</body>



</html>
