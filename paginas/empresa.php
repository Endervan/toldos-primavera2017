<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_cinza">
    <div class="row ">
      <div class="container">
        <div class="row ">
          <div class="col-6 top30 titulo_emp">
            <h6 class="Lato">CONHEÇA MAIS A</h6>
            <h6 class="left80"><span>TOLDOS PRIMAVERA</span></h6>


            <!-- ======================================================================= -->
            <!-- conheca nossa empresa  -->
            <!-- ======================================================================= -->
            <div class="top80 bottom40">
              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
              <div class="desc_empresa">
                <p><?php Util::imprime($row[descricao]); ?></p>
              </div>
            </div>
            <!-- ======================================================================= -->
            <!-- conheca nossa empresa  -->
            <!-- ======================================================================= -->
          </div>

          <div class="col-6 top220">


            <!-- ======================================================================= -->
            <!-- SLIDER CATEGORIA -->
            <!-- ======================================================================= -->
            <div class="col-12 slider_prod_geral">


              <!-- Place somewhere in the <body> of your page -->
              <div id="slider" class="flexslider">
                <!-- <span>Clique na imagem para Ampliar</span> -->
                <ul class="slides slider-prod">
                  <?php
                  $result = $obj_site->select("tb_banners", "AND tipo_banner = 3 ORDER BY rand() LIMIT 6");
                  if(mysql_num_rows($result) > 0)
                  {
                    while($row = mysql_fetch_array($result)){
                      ?>
                      <li class="zoom">
                        <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem]; ?>" class="group4">
                          <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 538, 409, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                          <!-- <img src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" class="input100" /> -->
                        </a>
                      </li>
                      <?php
                    }
                  }
                  ?>
                  <!-- items mirrored twice, total of 12 -->
                </ul>
              </div>
            </div>
            <!-- ======================================================================= -->
            <!-- SLIDER CATEGORIA -->
            <!-- ======================================================================= -->
            <div class="bloco_amarelo"></div>
          </div>

        </div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  PRODUTOS E SERVICOS -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      <div class="col-1"></div>
      <div class="col top50">
        <h6 >NOSSOS PRODUTOS E SERVIÇOS</h6>
      </div>
    </div>

    <div class="row">

      <?php
      $i=0;
      $result = $obj_site->select("tb_produtos","order by rand() limit 1");
      if(mysql_num_rows($result) > 0){

        while($row = mysql_fetch_array($result)){
          ?>

          <div class="col-4 top60">
            <div class="card">
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 409, 148, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="card-body text-center bg_amarelo_empresa">
                <div class="top25 text-uppercase"><h2 ><span><?php Util::imprime($row[titulo]); ?></span></h2></div>
                <div class="btn_sobreposto_empresa border-top-0">
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_empresa">CONFIRA NOSSOS MODELOS</a>
                </div>

              </div>


            </div>
          </div>
          <?php
          if($i == 2){
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }

        }
      }
      ?>


      <?php
      $i=0;
      $result = $obj_site->select("tb_servicos","order by rand() limit 2");
      if(mysql_num_rows($result) > 0){

        while($row = mysql_fetch_array($result)){
          ?>

          <div class="col-4 top60">
            <div class="card">
              <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 409, 148, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="card-body text-center bg_amarelo_empresa">
                <div class="top25 text-uppercase"><h2 ><span><?php Util::imprime($row[titulo]); ?></span></h2></div>
                <div class="btn_sobreposto_empresa_servico border-top-0">
                  <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_empresa_servico">MAIS DETALHES</a>
                </div>

              </div>


            </div>
          </div>
          <?php
          if($i == 1){
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }

        }
      }
      ?>


    </div>

  </div>
  <!-- ======================================================================= -->
  <!--  PRODUTOS E SERVICOS -->
  <!-- ======================================================================= -->




  <div class="container-fluid top75 bg_onde_empresa">
    <div class="row ">
      <div class="container">
        <div class="row ">
          <div class="col top105">
            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="363" frameborder="0" style="border:0" allowfullscreen></iframe>

            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->
          </div>
        </div>
      </div>

    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script>
$(window).load(function() {
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 100,
    itemMargin: 5,
    asNavFor: '#slider'
  });

  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});


</script>
