<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>



  <div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
  </div>

  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relative">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--   LISTAGEM DOS PRODUTOS -->
  <!--  ==============================================================  -->

  <div class="container">
    <div class="row">


  <div class="col-12 ">
    <h6 class="top50">PRODUTOS EM DESTAQUES</h6>

    <!-- ======================================================================= -->
    <!-- LISTA PRODUTO HOME    -->
    <!-- ======================================================================= -->
    <?php include('./includes/lista_produtos_home.php') ?>
    <!-- ======================================================================= -->
    <!-- LISTA PRODUTO HOME    -->
    <!-- ======================================================================= -->

  </div>

  <div class="clearfix"></div>


  <div class="col-12 text-right top40">
      <a class="btn btn-cinza" href="<?php echo Util::caminho_projeto() ?>/produtos">VER TODOS</a>
  </div>

</div>
</div>


<!--  ==============================================================  -->
<!--   NOSSOS SERVIÇOS -->
<!--  ==============================================================  -->
<div class="container-fluid top50 bg_cinza">
  <div class="row">

    <div class="container">
      <div class="row bg_servicos_home top70 bottom70">
        <div class="col-5 top145">
          <h6>NOSSOS SERVIÇOS</h6>
        </div>

        <div class="col-7">

          <?php
          $result = $obj_site->select("tb_servicos","order by rand() limit 1");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while($row = mysql_fetch_array($result)){

              ?>
              <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php echo Util::imprime($row[url_amigavel]) ?>">
                  <div class="btn btn_servicos_home text-uppercase top40"> <h3><?php Util::imprime($row[titulo]); ?></h3></div>
                  <div class="top25 left50">
                    <p><?php Util::imprime($row[descricao],500); ?></p>
                  </div>
              </a>

              <div class="ml-auto">
                <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais btn-lg top20 pull-right">SAIBA MAIS</a>
              </div>

              <?php
              if ($i==0) {
                echo "<div class='clearfix'>  </div>";
              }else {
                $i++;
              }
            }
          }
          ?>

        </div>

      </div>
    </div>

  </div>
</div>
<!--  ==============================================================  -->
<!--   NOSSOS SERVIÇOS -->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!--   empresa -->
<!--  ==============================================================  -->
<div class="container-fluid">
  <div class="row bg_empresa_home">

    <div class="container">
      <div class="row">
        <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>

        <div class="col-6 top35 text-center destaque_produto">
          <div class="text-uppercase"><h6 ><?php Util::imprime($row1[titulo]); ?></h6></div>
          <div class="top45 bottom50"><p ><?php Util::imprime($row1[descricao],327); ?></p></div>
          <div class="text-right bottom20">
            <a href="<?php echo Util::caminho_projeto() ?>/empresa" title="Saiba Mais" class="btn btn_saiba_mais">MAIS DETALHES</a>
          </div>
        </div>


      </div>
    </div>

  </div>
</div>
<!--  ==============================================================  -->
<!--   empresa -->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>





<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 300,
    itemMargin: 0,
    inItems: 1,
    maxItems: 3
  });
});
</script>
