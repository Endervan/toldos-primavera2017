<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",5) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_cinza">
    <div class="row ">
      <div class="container">
        <div class="row ">
          <div class="col-8 top30 titulo_emp">
            <h6 class="Lato">CONFIRA TODOS OS</h6>
            <h6 class="left80"><span>NOSSOS SERVICOS</span></h6>
          </div>


          <div class="col padding0 top40 procura_prod">
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
            <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
              <div class=" border-right-0 input-group input-group-lg">
                <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS">
                <span class="input-group-btn">
                  <button class="btn" type="submit"><span class="fa fa-search"></span></button>
                </span>
              </div>
            </form>
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!--SERVICO DENTRO-->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_cinza pb100">  
    <div class="row">
      <div class='container'>
        <div class="row top20">

          <div class="col-8 servico_Dentro">
            <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]",748, 218, array("class"=>"input100", "alt"=>"$dados_dentro[titulo]")) ?>
            <div class="top35">
              <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
            </div>

          </div>
          <div class="col-4 padding0">
            <h3 class="text-uppercase"><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h3>
            <div class="top20"><h4><?php Util::imprime($dados_dentro[subtitulo]); ?></h4></div>

            <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco" class="btn btn_servico_fale col-11" title="FALE CONOSCO,LIGUE AGORA">
              FALE CONOSCO,LIGUE AGORA
            </a>


            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->
            <div class="telefone_servico col-11 top20">
              <div class="media">
                <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_amarelo.png" alt="">
                <div class="media-body align-self-center">
                  <h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h2>
                </div>
              </div>
            </div>


            <?php if (!empty($config[telefone2])): ?>
              <div class="telefone_servico col-11">
                <div class="top10 media">
                  <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_amarelo.png" alt="">
                  <div class="media-body align-self-center">
                    <h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
                  </div>
                </div>
              </div>
            <?php endif; ?>



            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->


            <a href="javascript:void(0);" class="btn btn_servico col-11" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
              ADICIONAR AO ORÇAMENTO
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--SERVICO DENTRO-->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  NOSSOS PRODUTOS -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_cinza_claro">
    <div class="row  ">
      <div class="container">
        <div class="row ">
          <div class="col-3 padding0 top105 text-right titulo_produtos_servico">
            <h6 class="Lato">CONFIRA NOSSOS</h6>
            <h6><span>PRODUTOS</span></h6>
          </div>

          <?php
          $i = 0;
          $result = $obj_site->select("tb_produtos","order by rand() limit 3");
          if(mysql_num_rows($result) == 0){
            echo "<h2 class='bg-info clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
          }else{
            while ($row = mysql_fetch_array($result))
            {
              $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
              $row_categoria = mysql_fetch_array($result_categoria);
              ?>

              <div class="col-3 nossos_produtos top45">
                <div class="card">
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 262, 215, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                  </a>
                  <div class="card-body text-center bg_produtos_home1">
                    <div class="top20 desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[titulo],60); ?></h1></div>
                    <div class="btn_sobreposto1">
                      <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais1">MAIS DETALHES</a>

                      <a href="javascript:void(0);" class="btn_produtos_home" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_orcamento1.jpg" alt="">
                      </a>
                    </div>

                  </div>


                </div>
              </div>

              <?php
              if($i == 2){
                echo '<div class="clearfix"></div>';
                $i = 0;
              }else{
                $i++;
              }

            }
          }
          ?>


        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  NOSSOS PRODUTOS -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
