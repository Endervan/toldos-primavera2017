<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",8) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  z-index: 999 !important;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-8 top30 titulo_emp">
            <h6 class="Lato">ENTRE EM CONTATO</h6>
            <h6 class="left80"><span>TRABALHE CONOSCO</span></h6>
          </div>


          <div class="col padding0 top40 procura_prod">
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
            <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
              <div class=" border-right-0 input-group input-group-lg">
                <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS">
                <span class="input-group-btn">
                  <button class="btn" type="submit"><span class="fa fa-search"></span></button>
                </span>
              </div>
            </form>
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <div class="container-fluid  bg_cinza pb300 ">
    <div class="row">

      <div class="container top65 relativo">
          <div class="row ">

            <div class="col-4">

              <div class="text-center bottom20">
                <h3 class="text-uppercase">solicite informações ou tire suas dúvidas.</h3>
              </div>

              <ul class="nav nav-pills text-center flex-column">
                <li class="nav-item  col-12">
                  <a class="nav-link " href="<?php echo Util::caminho_projeto() ?>/fale-conosco">
                    <img class="pull-left right10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_fale.png" alt="">FALE CONOSCO
                  </a>
                </li>
                <li class="nav-item  col-12 top10">
                  <a class="nav-link active" href="#">
                    <img  class="pull-left right10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_trabalhe.png" alt="">TRABALHE CONOSCO
                  </a>
                </li>
                <li class="nav-item  col-12 top10">
                  <a class="nav-link" href="#"  onclick="$('html,body').animate({scrollTop: $('.mapa_trabalhe').offset().top}, 1000);">
                    <img  class="pull-left right10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_local.png" alt="">  ONDE ESTAMOS
                  </a>
                </li>

              </ul>

              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              <div class="telefone_servico ml-3 col-10 top20">
                <div class="media">
                  <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_amarelo.png" alt="">
                  <div class="media-body align-self-center">
                    <h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h2>
                  </div>
                </div>
              </div>


              <?php if (!empty($config[telefone2])): ?>
                <div class="telefone_servico ml-3 col-10">
                  <div class="top10 media">
                    <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_amarelo.png" alt="">
                    <div class="media-body align-self-center">
                      <h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
                    </div>
                  </div>
                </div>
              <?php endif; ?>


            </div>

            <!--  ==============================================================  -->
            <!-- FORMULARIO CONTATOS-->
            <!--  ==============================================================  -->

            <div class="col-8 fundo_formulario">

              <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">

              <div class="form-row">
                <div class="col">
                  <div class="form-group icon_form">
                    <input type="text" name="nome" class="form-control fundo-form form-control-lg input-lg" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="email" class="form-control fundo-form form-control-lg input-lg" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback"></span>
                  </div>
                </div>
              </div>

              <div class="form-row">
                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="telefone" class="form-control fundo-form form-control-lg input-lg" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="escolaridade" class="form-control fundo-form form-control-lg input-lg" placeholder="ESCOLARIDADE">
                    <span class="fa fa-graduation-cap form-control-feedback"></span>
                  </div>
                </div>
              </div>

              <div class="form-row">
                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="area" class="form-control fundo-form form-control-lg input-lg" placeholder="ÁREA">
                    <span class="fa fa-briefcase form-control-feedback"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="cargo" class="form-control fundo-form form-control-lg input-lg" placeholder="CARGO">
                    <span class="fa fa-address-book form-control-feedback"></span>
                  </div>
                </div>
              </div>

              <div class="form-row">
                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="cidade" class="form-control fundo-form form-control-lg input-lg" placeholder="CIDADE">
                    <span class="fa fa-globe form-control-feedback"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="estado" class="form-control fundo-form form-control-lg input-lg" placeholder="ESTADO">
                    <span class="fa fa-globe form-control-feedback"></span>
                  </div>
                </div>
              </div>

              <div class="form-row">

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="file" name="curriculo" class="form-control fundo-form form-control-lg input-lg" placeholder="CURRICULO">
                    <span class="fa fa-file-text form-control-feedback"></span>
                  </div>
                </div>
              </div>


              <div class="form-row">
                <div class="col">
                  <div class="form-group icon_form">
                    <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="MENSAGEM"></textarea>
                    <span class="fa fa-pencil form-control-feedback"></span>
                  </div>
                </div>
              </div>


              <div class="col-12 text-right">
                <button type="submit" class="btn btn_formulario" name="btn_contato">
                  ENVIAR
                </button>
              </div>


            </div>

          </div>
        </form>
        <!--  ==============================================================  -->
        <!-- FORMULARIO CONTATOS-->
        <!--  ==============================================================  -->

        <div class="col mapa_trabalhe">
          <!-- ======================================================================= -->
          <!-- mapa   -->
          <!-- ======================================================================= -->
          <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="590" frameborder="0" style="border:0" allowfullscreen></iframe>

          <!-- ======================================================================= -->
          <!-- mapa   -->
          <!-- ======================================================================= -->
        </div>



      </div>


    </div>
  </div>


  <div class='container-fluid'>
    <div class="row ">
      <div class="bg_branco_alt"></div>
    </div>
  </div>



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>



    <?php
    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_POST[nome]))
    {

      if(!empty($_FILES[curriculo][name])):
        $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
        $texto = "Anexo: ";
        $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
        $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
      endif;

      $texto_mensagem = "
      Nome: ".$_POST[nome]." <br />
      Email: ".$_POST[email]." <br />
      Telefone: ".$_POST[telefone]." <br />
      Sexo: ".$_POST[sexo]." <br />
      Nascimento: ".$_POST[nascimento]." <br />
      Grau de Instrução: ".$_POST[grau]." <br />
      Endereço: ".$_POST[endereco]." <br />
      Area Pretentida: ".$_POST[area]." <br />
      Cargo: ".$_POST[cargo]." <br />

      Mensagem: <br />
      ".nl2br($_POST[mensagem])."

      <br><br>
      $texto
      ";


      Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
      Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
      Util::alert_bootstrap("Obrigado por entrar em contato.");
      unset($_POST);
    }

    ?>




    <script>
    $(document).ready(function() {
      $('.FormContatos').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'fa fa-check',
          invalid: 'fa fa-remove',
          validating: 'fa fa-refresh'
        },
        fields: {
          nome: {
            validators: {
              notEmpty: {
                message: 'Informe nome.'
              }
            }
          },
          email: {
            validators: {
              notEmpty: {
                message: 'Seu email.'
              },
              emailAddress: {
                message: 'Esse endereço de email não é válido'
              }
            }
          },
          telefone: {
            validators: {
              notEmpty: {
                message: 'Por favor adicione seu numero.'
              },
              phone: {
                country: 'BR',
                message: 'Informe um telefone válido.'
              }
            },
          },
          assunto: {
            validators: {
              notEmpty: {

              }
            }
          },
          endereco: {
            validators: {
              notEmpty: {

              }
            }
          },
          cidade: {
            validators: {
              notEmpty: {
                message: 'Por favor adicione sua Cidade.'
              }
            }
          },
          estado: {
            validators: {
              notEmpty: {
                message: 'Por favor adicione seu Estado.'
              }
            }
          },
          escolaridade1: {
            validators: {
              notEmpty: {
                message: 'Sua Informação Acadêmica.'
              }
            }
          },
          curriculo: {
            validators: {
              notEmpty: {
                message: 'Por favor insira seu currículo'
              },
              file: {
                extension: 'doc,docx,pdf,rtf',
                type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                maxSize: 5*1024*1024,   // 5 MB
                message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
              }
            }
          },
          mensagem: {
            validators: {
              notEmpty: {
                message: 'Insira sua Mensagem.'
              }
            }
          }
        }
      });
    });
    </script>


    <!-- ======================================================================= -->
    <!-- MOMENTS  -->
    <!-- ======================================================================= -->
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript">

    $('#hora').datetimepicker({
      format: 'LT'
    });

    </script>
