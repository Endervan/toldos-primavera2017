<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row ">
      <div class="container">
        <div class="row ">
          <div class="col-8 top30 titulo_emp">
            <h6 class="Lato">CONFIRA TODOS OS</h6>
            <h6 class="left80"><span>NOSSOS PRODUTOS</span></h6>
          </div>


        </div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->







  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">





      <div class="col-12 top15 padding0">

        <?php

        $url1 = Url::getURL(1);
        $url2 = Url::getURL(2);

        //  FILTRA AS CATEGORIAS
        if (isset( $url1 )) {
          $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
          $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
        }


        //  FILTRA AS SUBCATEGORIAS
        if (isset( $url2 )) {
          $id_subcategoria = $obj_site->get_id_url_amigavel("tb_subcategorias_produtos", "idsubcategoriaproduto", $url2);
          $complemento .= "AND id_subcategoriaproduto = '$id_subcategoria' ";
        }



        //  FILTRA PELO TITULO
        if(isset($_POST[id_categoriaproduto]) and !empty($_POST[id_categoriaproduto])):
          $complemento .= "AND id_categoriaproduto = '$_POST[id_categoriaproduto]' ";
        endif;

        //  FILTRA PELO TITULO
        if(isset($_POST[id_subcategoriaproduto]) and !empty($_POST[id_subcategoriaproduto]) ):
          $complemento .= "AND id_subcategoriaproduto = '$_POST[id_subcategoriaproduto]' ";
        endif;

        //  FILTRA PELO TITULO
        if(isset($_POST[busca_produtos])):
          $complemento .= "AND titulo LIKE '%$_POST[busca_produtos]%'";
        endif;

        ?>

        <?php

        //  busca os produtos sem filtro
        $result = $obj_site->select("tb_produtos", $complemento);

        if(mysql_num_rows($result) == 0){
          echo "
          <div class=' col-12 text-center '>
          <h2 class='btn_nao_encontrado' style='padding: 20px;'>Nenhum produto encontrado.</h2>
          </div>"
          ;
        }else{
          ?>
          <div class="col-12 total-resultado-busca">
            <h1><span><?php echo mysql_num_rows($result) ?></span> PRODUTO(S) ENCONTRADO(S) . </h1>
          </div>

          <?php
          require_once('./includes/lista_produtos.php');

        }
        ?>

      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->



  <div class="top195"></div>

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
