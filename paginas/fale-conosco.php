<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  z-index: 999 !important;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-8 top30 titulo_emp">
            <h6 class="Lato">ENTRE EM CONTATO</h6>
            <h6 class="left80"><span>FALE CONOSCO</span></h6>
          </div>


          <div class="col padding0 top40 procura_prod">
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
            <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
              <div class=" border-right-0 input-group input-group-lg">
                <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS">
                <span class="input-group-btn">
                  <button class="btn" type="submit"><span class="fa fa-search"></span></button>
                </span>
              </div>
            </form>
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <div class="container-fluid  bg_cinza pb300 ">
    <div class="row">

      <div class="container top65 relativo">
        <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">
          <div class="row ">

            <div class="col-4">

              <div class="text-center bottom20">
                <h3 class="text-uppercase">solicite informações ou tire suas dúvidas.</h3>
              </div>

              <ul class="nav nav-pills text-center flex-column">
                <li class="nav-item  col-12">
                  <a class="nav-link active" href="#">
                    <img class="pull-left right10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_fale.png" alt="">FALE CONOSCO
                  </a>
                </li>
                <li class="nav-item  col-12 top10">
                  <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">
                    <img  class="pull-left right10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_trabalhe.png" alt="">TRABALHE CONOSCO
                  </a>
                </li>
                <li class="nav-item  col-12 top10">
                  <a class="nav-link" href="#"  onclick="$('html,body').animate({scrollTop: $('.mapa').offset().top}, 1000);">
                    <img  class="pull-left right10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_local.png" alt="">  ONDE ESTAMOS
                  </a>
                </li>

              </ul>

              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              <div class="telefone_servico ml-3 col-10 top20">
                <div class="media">
                  <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_amarelo.png" alt="">
                  <div class="media-body align-self-center">
                    <h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h2>
                  </div>
                </div>
              </div>


              <?php if (!empty($config[telefone2])): ?>
                <div class="telefone_servico ml-3 col-10">
                  <div class="top10 media">
                    <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_amarelo.png" alt="">
                    <div class="media-body align-self-center">
                      <h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
                    </div>
                  </div>
                </div>
              <?php endif; ?>


            </div>

            <!--  ==============================================================  -->
            <!-- FORMULARIO CONTATOS-->
            <!--  ==============================================================  -->
            <div class="col-8 fundo_formulario">

              <div class="form-row">
                <div class="col">
                  <div class="form-group icon_form">
                    <input type="text" name="nome" class="form-control fundo-form form-control-lg input-lg" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="email" class="form-control fundo-form form-control-lg input-lg" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback"></span>
                  </div>
                </div>
              </div>

              <div class="form-row">
                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="telefone" class="form-control fundo-form form-control-lg input-lg" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="assunto" class="form-control fundo-form form-control-lg input-lg" placeholder="ASSUNTO">
                    <span class="fa fa-star form-control-feedback"></span>
                  </div>
                </div>
              </div>


              <div class="form-row">
                <div class="col">
                  <div class="form-group icon_form">
                    <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="MENSAGEM"></textarea>
                    <span class="fa fa-pencil form-control-feedback"></span>
                  </div>
                </div>
              </div>


              <div class="col-12 text-right padding0 ">
                <button type="submit" class="btn btn_formulario" name="btn_contato">
                  ENVIAR
                </button>
              </div>


            </div>

          </div>
        </form>
        <!--  ==============================================================  -->
        <!-- FORMULARIO CONTATOS-->
        <!--  ==============================================================  -->

        <div class="col mapa">
          <!-- ======================================================================= -->
          <!-- mapa   -->
          <!-- ======================================================================= -->
          <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="590" frameborder="0" style="border:0" allowfullscreen></iframe>

          <!-- ======================================================================= -->
          <!-- mapa   -->
          <!-- ======================================================================= -->
        </div>



      </div>


    </div>
  </div>


  <div class='container-fluid'>
    <div class="row ">
      <div class="bg_branco_alt"></div>
    </div>
  </div>



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
  echo $texto_mensagem = "
  Nome: ".($_POST[nome])." <br />
  Email: ".($_POST[email])." <br />
  Telefone: ".($_POST[telefone])." <br />
  Assunto: ".($_POST[assunto])." <br />

  Mensagem: <br />
  ".(nl2br($_POST[mensagem]))."
  ";

  ;




  Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}


?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Insira seu nome.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {
            message: 'Insira sua Mensagem.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Informe um email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor informe seu numero!.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
