<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background: #fffff7  url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row ">
      <div class="container">
        <div class="row ">
          <div class="col-8 top30 titulo_emp">
            <h6 class="Lato">CONFIRA TODOS OS</h6>
            <h6 class="left80"><span>NOSSOS SERVICOS</span></h6>
          </div>


          <div class="col padding0 top40 procura_prod">
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
            <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
              <div class=" border-right-0 input-group input-group-lg">
                <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS">
                <span class="input-group-btn">
                  <button class="btn" type="submit"><span class="fa fa-search"></span></button>
                </span>
              </div>
            </form>
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->




  <!--  ==============================================================  -->
  <!--   NOSSOS SERVIÇOS -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row servicos">

      <?php
      $result = $obj_site->select("tb_servicos");
      if(mysql_num_rows($result) > 0){
        $i = 0;
        while($row = mysql_fetch_array($result)){
          if ($i == 0) {
            $order = 'order-1';
            $order1 = 'order-12';
            $div = 'container_metade_esquerda';
            $i++;
          }else{
            $order = 'order-12';
            $order1 = 'order-1';
            $div = 'container_metade_direita';
            $i = 0;
          }
          ?>

          <div class="relativo">
            <div class="<?php echo $div; ?>"></div>
            <div class="row">
              <div class="col-5 top70 <?php echo $order;?>">
                <h3 class="text-uppercase top15"><span><?php Util::imprime($row[titulo]); ?></span></h3>
                <div class="top20"><h4><?php Util::imprime($row[subtitulo]); ?></h4></div>
                <div class="top20 desc-servico"><p><span><?php Util::imprime($row[descricao],350); ?></span></p></div>
              </div>
              <div class="col-7 <?php echo $order1;?> top15">
                <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 745, 215, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>
                </a>
              </div>
            </div>
          </div>

          <div class="col-12 bottom40 top10 text-center">
            <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais_servicos">MAIS DETALHES</a>
          </div>

          <?php
          if ($i==0) {
            echo "<div class='clearfix'>  </div>";
          }else {
            $i++;
          }
        }
      }
      ?>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   NOSSOS SERVIÇOS -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!--  NOSSOS PRODUTOS -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_cinza_claro">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-3 padding0 top105 text-right titulo_produtos_servico">
            <h6 class="Lato">CONFIRA NOSSOS</h6>
            <h6><span>PRODUTOS</span></h6>
          </div>

          <?php
          $i = 0;
          $result = $obj_site->select("tb_produtos","order by rand() limit 3");
          if(mysql_num_rows($result) == 0){
            echo "<h2 class='bg-info clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
          }else{
            while ($row = mysql_fetch_array($result))
            {
              $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
              $row_categoria = mysql_fetch_array($result_categoria);
              ?>

              <div class="col-3 nossos_produtos top45">
                <div class="card">
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 262, 215, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                  </a>
                  <div class="card-body text-center bg_produtos_home1">
                    <div class="top20 desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[titulo],60); ?></h1></div>
                    <div class="btn_sobreposto1">
                      <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais1">MAIS DETALHES</a>

                      <a href="javascript:void(0);" class="btn_produtos_home" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_orcamento1.jpg" alt="">
                      </a>
                    </div>

                  </div>


                </div>
              </div>

              <?php
              if($i == 2){
                echo '<div class="clearfix"></div>';
                $i = 0;
              }else{
                $i++;
              }

            }
          }
          ?>


        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  NOSSOS PRODUTOS-->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
